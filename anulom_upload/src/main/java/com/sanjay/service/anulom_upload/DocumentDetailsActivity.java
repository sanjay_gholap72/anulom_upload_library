package com.sanjay.service.anulom_upload;



import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;

public class DocumentDetailsActivity extends AppCompatActivity implements View.OnClickListener
{
    private EditText edtDocumentID,edtDocumentDescription;
    private Button btnChooseImage;
    private String userMail;
    private String executorUID,executorName;
    private String documentID,documentDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_details);

        Bundle bundle = getIntent().getExtras();
        if(bundle !=null)
        {
            userMail = bundle.getString("EmailID");
        }

        edtDocumentID =(EditText) findViewById(R.id.edtxt_docid);
        edtDocumentDescription = (EditText) findViewById(R.id.edtxt_docdes);
        btnChooseImage = (Button)findViewById(R.id.btn_choose);

        btnChooseImage.setOnClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        if(v == btnChooseImage)
        {
            ChooseImage();
        }
    }


    private void ChooseImage()
    {
        documentID = edtDocumentID.getText().toString().trim();
        documentDescription =  edtDocumentDescription.getText().toString().trim();

        if(documentID.isEmpty())
        {
            Snackbar.make(findViewById(android.R.id.content),"Document ID can not blank..", Snackbar.LENGTH_SHORT).show();
        }
        else if(documentDescription.isEmpty())
        {
            Snackbar.make(findViewById(android.R.id.content),"Document description can not blank..", Snackbar.LENGTH_SHORT).show();
        }
        else
        {
            if(documentID.contains("_"))
            {
                Intent intent = new Intent(DocumentDetailsActivity.this, ImageChooser.class);
                intent.putExtra("documentID", documentID);
                intent.putExtra("documentDescription", documentDescription);
                intent.putExtra("EmailID", userMail);
                startActivity(intent);
            }
            else
            {
                Snackbar.make(findViewById(android.R.id.content),"Please check document id", Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}